import { greet } from '../index';

describe('greet', () => {
	it.each`
		input                                                     | outcome
		${'Bob'}                                                  | ${'Hello, Bob.'}
		${null}                                                   | ${'Hello, my friend.'}
		${'JERRY'}                                                | ${'HELLO JERRY!'}
		${['Jill', 'Jane']}                                       | ${'Hello, Jill and Jane.'}
		${['Amy', 'Brian', 'Charlotte']}                          | ${'Hello, Amy, Brian, and Charlotte.'}
		${['Amy', 'BRIAN', 'Charlotte']}                          | ${'Hello, Amy and Charlotte. AND HELLO BRIAN!'}
		${['Bob', 'Charlie, Dianne']}                             | ${'Hello, Bob, Charlie, and Dianne.'}
		${['Bob', '"Charlie, Dianne"']}                           | ${'Hello, Bob and Charlie, Dianne.'}
		${['BRIAN', 'Amy', 'Charlotte', 'John', 'JIMMY']}         | ${'Hello, Amy, Charlotte, and John. AND HELLO BRIAN AND JIMMY!'}
		${['Amy', 'BRIAN', 'FRED', 'Charlotte', 'John', 'JIMMY']} | ${'Hello, Amy, Charlotte, and John. AND HELLO BRIAN, FRED, AND JIMMY!'}
		${['Amy', 'BRIAN', 'FRED']}                               | ${'Hello, Amy. AND HELLO BRIAN AND FRED!'}
		${['Amy', 'BRIAN', 'FRED', 'JOE']}                        | ${'Hello, Amy. AND HELLO BRIAN, FRED, AND JOE!'}
		${['Amy', 'BRIAN, FRED']}                                 | ${'Hello, Amy. AND HELLO BRIAN AND FRED!'}
		${['Amy', '"BRIAN, FRED"']}                               | ${'Hello, Amy. AND HELLO BRIAN, FRED!'}
	`('shoud handle all combinations of names', ({ input, outcome }) => {
		const greeting = greet(input);
		expect(greeting).toBe(outcome);
	});
});
