const isUpperCase = str => {
	return str === str.toString().toUpperCase();
};

const handleArray = inputs => {
	return `${inputs.slice(0, inputs.length - 1).join(', ')}${
		inputs.length > 2 ? ',' : ''
	}${inputs.length > 1 ? ' and ' : ''}${inputs[inputs.length - 1]}`.replace(
		/  /g,
		' '
	);
};

const handleCase = input => {
	return {
		upper: input.filter(i => isUpperCase(i)),
		lower: input.filter(i => !isUpperCase(i))
	};
};

const handleCommas = input => {
	return input.reduce((acc, item) => {
		var split =
			item.indexOf('"') > -1 ? [item.replace(/\"/g, '')] : item.split(',');
		return [...acc, ...split];
	}, []);
};

const handleGreeting = ({ upper, lower }) => {
	let greeting = '';

	if (lower.length > 0) {
		greeting = `Hello, ${handleArray(lower)}.`;
	}

	if (upper.length > 0) {
		greeting = `${greeting}${
			lower.length > 0 ? ' AND ' : ''
		}HELLO ${handleArray(upper).toUpperCase()}!`;
	}

	return greeting;
};

const greet = input => {
	if (input === null) return 'Hello, my friend.';

	input = Array.isArray(input) ? input : [input];
	input = handleCommas(input);

	const { lower, upper } = handleCase(input);
	return handleGreeting({ lower, upper });
};

export { greet };
